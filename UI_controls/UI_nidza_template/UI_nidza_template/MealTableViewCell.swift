//
//  MealTableViewCell.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/2/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import UIKit

class MealTableViewCell: UITableViewCell {

// MARK: Properties
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var detail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        print("SELECTED CELL INIT")
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        print("SELECTED CELL")
    }
   
}
