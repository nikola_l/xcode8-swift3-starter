//
//  canvasViewController.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/8/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import UIKit

class canvasViewController: UIViewController {

    
    @IBOutlet weak var alert1: UIButton!
    @IBOutlet weak var Canvas: UIView!
    
    
    
    var DEVICE = DEVICE_INFO()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
  
   // Canvas.frame.origin.x = 0
   // Canvas.frame.origin.y = 0
    
    

    @IBAction func BACK_TO_MAIN(_ sender: AnyObject) {
        
        print("BAck to main storyboard")
        
        let viewController1:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MAIN_FORM") as UIViewController
        self.present(viewController1, animated: false, completion: nil)
        
    }
    
    @IBAction func showalert1(_ sender: AnyObject) {
       
        
        var uiAlert = UIAlertController(title: "Title", message: "Message from nidzsa", preferredStyle: UIAlertControllerStyle.alert)
        self.present(uiAlert, animated: true, completion: nil)
        
        uiAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            print("Click of default button")
        }))
        
        uiAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Click of cancel button")
        }))

        
         Canvas.frame.origin.x = 0
         Canvas.frame.origin.y = 0
         Canvas.frame.size.width = DEVICE.W()
        
        
    }
    ///////////////////
    // Touch , double touch
    ///////////////////
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            
            print(touch.tapCount )
    
            if #available(iOS 9.1, *) {
                print( touch.preciseLocation(in: self.view) )
            } else {
                // Fallback on earlier versions
            }
            
            var pos = touch.location(in: self.view )
            
            print("TOUCH -- this is position x " , pos.x)
            print("TOUCH -- this is position y " , pos.y)
            
            
        }
        else {
        
            print("TOUCH with secund hand or finger !!!!!!!!!!  " )
        
        
        }
        
        
        super.touchesBegan(touches, with: event)
    }
    
    
    ///////////////////
    // touchMOVE
    ///////////////////
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let pos = touch.location(in: self.view)
            // do something with your currentPoint
            print("TOUCHMOVE -- this is position x " , pos.x)
            print("TOUCHMOVE -- this is position y " , pos.y)

        }
    }

    
    
    
    
    
    
}
