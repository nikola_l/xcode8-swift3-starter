//
//  Alert_Exstension.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/12/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    
    
    
    
    //func show alert
       func AlertOk( title : String , msg : String ) {
      //func AlertOk( title : String , msg : String )-> UIAlertController{
        
        
        
        
        let uiAlert = UIAlertController (title: title , message:  msg , preferredStyle: UIAlertControllerStyle.alert)
        self.present(  uiAlert  , animated: true, completion: nil)
        
        
       // uiAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
         //   print("Click on ok button")
       // }
       // ))
        
       // return uiAlert;
        
    }
    
    

      func alertWithTitle(title: String, message: String, buttonTitle: String) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonTitle, style: .default, handler: nil)
        alertController.addAction(action)
        
        return alertController
    }


}
