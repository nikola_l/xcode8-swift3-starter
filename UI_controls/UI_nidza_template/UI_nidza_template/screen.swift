//
//  screen.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/8/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import Foundation
import UIKit

class DEVICE_INFO {
    
    //////////////////////
    // MARK: Properties
    var device_type: String = ""
    let screenSize: CGRect = UIScreen.main.bounds
    
    
    
    /////////////////////
    // GET
    func WIDTH() -> CGFloat {
        let screenWidth = screenSize.width
        return screenWidth
    }
    func W() -> CGFloat {
        let screenWidth = screenSize.width
        return screenWidth
    }
    
    func HEIGHT()->CGFloat {
        let screenHeight = screenSize.height
        return screenHeight
    }
    
    func H()->CGFloat {
        let screenHeight = screenSize.height
        return screenHeight
    }
    
    //////////////////
    // get per perc
    func W( per : Float ) -> CGFloat {
        
        var screenWidth1 = Float( screenSize.width)
        screenWidth1 = (screenWidth1 / 100 ) * per
        return CGFloat (screenWidth1)
    }
    
    func H( per : Float ) -> CGFloat {
        
        var screenHeight1 = Float( screenSize.height)
        screenHeight1 = (screenHeight1 / 100 ) * per
        return CGFloat ( screenHeight1 )
    }
    
    
    
    
    
    
}
