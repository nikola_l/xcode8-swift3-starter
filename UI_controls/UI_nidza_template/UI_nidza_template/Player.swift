//
//  Player.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/19/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class MP3PLAYER {
    
    
    var player = AVPlayer()
    // var playbackLikelyToKeepUpContext = UnsafeMutableRawPointer?
    var playbackLikelyToKeepUpContext: UnsafeMutableRawPointer?
    var playbackBufferEmpty: UnsafeMutableRawPointer?
    
    
    func PLAY (){
        
        player.play()
        
    }
    
    init ( url_ : String , autoplay_ : String ) {
        
        
        
        let url = url_
        
        let playerItem = AVPlayerItem( url:URL( string:url )! )
        player = AVPlayer(playerItem:playerItem)
        player.rate = 1.0;
        
        
        if autoplay_ == "YES" {
            player.play()
            print("autoplay is on")
        }
        
        //@@@@@@@@@@@@@@@@@@@@@@@
        // OBS
        //////////////////////////
        
        // player.addObserver(self, forKeyPath: "status", options:NSKeyValueObservingOptions(), context: nil)
        // player.addObserver(self, forKeyPath: "playbackBufferEmpty", options:NSKeyValueObservingOptions(), context: nil)
        // player.currentItem?.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options:NSKeyValueObservingOptions(), context: nil)
        // player.addObserver(self, forKeyPath: "loadedTimeRanges", options: NSKeyValueObservingOptions(), context: nil)
        
        
        
    }

/*
    
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        // Only handle observations for the playerItemContext
        print("OBSERVE PLAYER")
        
        guard context == &playbackLikelyToKeepUpContext else {
            
            if (context != nil){
                super.observeValue(forKeyPath: keyPath,
                                   of: object,
                                   change: change,
                                   context: context)
            }
            
           
            // .text = "playing ";
            print("OBSERVE::::" , keyPath)
            
            return
            
            
        }
      
   
 
        
    }
      */
    
    private func deallocObservers(player_: AVPlayer) {
        
        //  player.removeObserver(self, forKeyPath: "status")
        //  player.removeObserver(self, forKeyPath: "playbackBufferEmpty")
        //  player.currentItem?.removeObserver(self, forKeyPath: "playbackLikelyToKeepUp")
        
    }
    
    
    
}
