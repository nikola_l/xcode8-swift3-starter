//
//  webBrowser_actions.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/12/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import Foundation
import UIKit


public extension UIWebView {
    
    
    func LoadPage ( address : String ) {
        
        let url = NSURL (string: address);
        let requestObj = NSURLRequest(url: url! as URL);
        self.loadRequest(requestObj as URLRequest);
        
    }
    
    func loadLocalHTML( localHtmlFile : String) {
        
        let localfilePath = Bundle.main.url(forResource: localHtmlFile  , withExtension: "html");
        let myRequest = NSURLRequest(url: localfilePath!);
        self.loadRequest(myRequest as URLRequest);
        
    }
    
    func loadText( htmlText : String) {
        
        self.loadHTMLString(htmlText, baseURL: nil)
        
    }
    
    
}
