//
//  web_functions.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/8/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import Foundation



import UIKit

class WEB {
    
    
    
    func DownloadImage( link : String )->UIImage {
        
        let url = NSURL(string: link )
        let data = NSData(contentsOf: url! as URL) //make sure your image in this url does exist, otherwise unwrap in a if let check
        
        var image = UIImage()
        image = UIImage(data: data! as Data)!
        
        return image
        
    }
    
    
    
    func GetContentFromWeb ( link_ : String) {
    
    
       // let myURLString = "https://google.com"
        guard let myURL = URL(string: link_) else {
            print("Error: \(link_) doesn't seem to be a valid URL")
            return
        }
        
        do {
            let link_ = try String(contentsOf: myURL, encoding: .ascii)
            print("HTML : \(link_)")
        } catch let error {
            print("Error: \(error)")
        }
        
    }
    
    
}
