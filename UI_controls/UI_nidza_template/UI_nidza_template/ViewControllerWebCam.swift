//
//  ViewControllerWebCam.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/9/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//


import UIKit
import AVFoundation


class ViewControllerWebCam: UIViewController {
    
    var DEVICE = DEVICE_INFO()
    
    let captureSession = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer?
    
    @IBOutlet weak var resizeBTN: UIButton!
    @IBOutlet weak var preview_WC: UIView!
    @IBOutlet weak var button2: UIButton!
    
    // If we find a device we'll store it here for later use
    var captureDevice : AVCaptureDevice?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        captureSession.sessionPreset = AVCaptureSessionPresetHigh
        
        let devices = AVCaptureDevice.devices()
        
        // Loop through all the capture devices on this phone
        for device in devices! {
            // Make sure this particular device supports video
            if ((device as AnyObject).hasMediaType(AVMediaTypeVideo)) {
                // Finally check the position and confirm we've got the back camera
                if((device as AnyObject).position == AVCaptureDevicePosition.back) {
                    captureDevice = device as? AVCaptureDevice
                    if captureDevice != nil {
                        print("Capture device found")
                        beginSession()
                    }
                }
            }
        }
        
    }
    
    func focusTo(value : Float) {
        if let device = captureDevice {
            do {
                try device.lockForConfiguration()
                device.setFocusModeLockedWithLensPosition(value, completionHandler: { (time) -> Void in
                    //
                })
                device.unlockForConfiguration()
            } catch let error as NSError {
                print(error.code)
            }
        }
    }
    
    let screenWidth = UIScreen.main.bounds.size.width
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let screenSize = previewLayer!.bounds.size
        let frameSize:CGSize = view.frame.size
        if let touchPoint = touches.first {
            
            let location:CGPoint = touchPoint.location(in: self.view)
            
            let x = location.x / frameSize.width
            let y = 1.0 - (location.x / frameSize.width)
            
            let focusPoint = CGPoint(x: x, y: y)
            
            print("POINT : X: \(x), Y: \(y)")
            
            
            let captureDevice = (AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) as! [AVCaptureDevice]).filter{$0.position == .back}.first
            
            if let device = captureDevice {
                do {
                    try device.lockForConfiguration()
                    
                    let support:Bool = device.isFocusPointOfInterestSupported
                    
                    if support  {
                        
                        print("focusPointOfInterestSupported: \(support)")
                        
                        device.focusPointOfInterest = focusPoint
                        
                        // device.focusMode = .ContinuousAutoFocus
                        device.focusMode = .autoFocus
                        // device.focusMode = .Locked
                        
                        device.unlockForConfiguration()
                        
                        print("Focus point was set successfully")
                    }
                    else{
                        print("focusPointOfInterestSupported is not supported: \(support)")
                    }
                }
                catch {
                    // just ignore
                    print("Focus point error")
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first{
            // print("\(touch)")
        }
        super.touchesEnded(touches, with: event)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first{
            // print("\(touch)")
        }
        super.touchesMoved(touches, with: event)
    }
    
    
    func configureDevice() {
        if let device = captureDevice {
            do {
                try device.lockForConfiguration()
                device.focusMode = .autoFocus
                device.unlockForConfiguration()
            } catch let error as NSError {
                print(error.code)
            }
        }
        
    }
    
    
    func beginSession() {
        
        configureDevice()
        
        try! captureSession.addInput(AVCaptureDeviceInput(device: captureDevice))
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        previewLayer?.frame =  preview_WC.layer.frame
        previewLayer?.bounds = preview_WC.bounds
        preview_WC.layer.addSublayer(previewLayer!)
        // self.view.layer.addSublayer(previewLayer!)
        
        captureSession.startRunning()
        
        
    }
    
    
    @IBAction func resizeBTN__(_ sender: AnyObject) {
        
        //previewLayer?.bounds.origin.x = 0
        //previewLayer?.bounds.origin.y = 0
        print("postavi resize ")
        //previewLayer?.frame.origin.x = 0
        //previewLayer?.frame.origin.y = 0
        //previewLayer?.frame.size.width = DEVICE.W()
        
        var  bounds  = preview_WC.layer.bounds;
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill;
        previewLayer?.bounds=bounds;
        previewLayer?.position=CGPoint(x:bounds.midX,y: bounds.midY);
        
        
    }
    
    
    @IBAction func clickOnBTN2(_ sender: AnyObject) {
        
        //button2.
        
        button2.frame.size.width =   DEVICE.W(per: 50)
        button2.frame.size.height =    DEVICE.H(per: 11)
        
        button2.frame.origin.x = DEVICE.W(per: 50)
        button2.frame.origin.y = DEVICE.H(per: 80)
        
        var NN =  DEVICE.W(per: 20) + DEVICE.W(per: 20)
        print (NN , "   <><><> " ,  DEVICE.W(per: 20))
        
        
    }
    
    
}

