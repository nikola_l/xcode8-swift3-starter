//
//  HUD_inTable.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/2/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import UIKit

class HUD_inTable: UITableViewCell {

   
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var title: UIButton!
    @IBOutlet weak var detail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
