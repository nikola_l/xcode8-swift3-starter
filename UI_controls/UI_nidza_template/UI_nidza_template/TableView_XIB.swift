//
//  TableView_XIB.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/1/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//
import UIKit
import MobileCoreServices
import AVFoundation
class TableView_XIB: UIViewController ,  UITableViewDelegate, UITableViewDataSource
{
 
    @IBOutlet weak var table2: UITableView!
    @IBOutlet weak var table1: UITableView!
    var meals = [Meal]()
    
    
    func loadSampleMeals() {
        
        let photo1 = UIImage(named: "fire1_1")!
        let meal1 = Meal(name: "Caprese Salad", photo: photo1, rating: 4)!
        
        let photo2 = UIImage(named: "fire1_2")!
        let meal2 = Meal(name: "Chicken and Potatoes", photo: photo2, rating: 5)!
        
        let photo3 = UIImage(named: "fire1_3")!
        let meal3 = Meal(name: "Pasta with Meatballs", photo: photo3, rating: 3)!
        
        meals += [meal1, meal2, meal3]
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         loadSampleMeals()
        
        // ONLOAD
        
        //table1.delegate = self
        //table1.dataSource = self  // DELEGATED FROM DESINGER
        
        table2.delegate = self      // DELEGATE FROM CODE
        table2.dataSource = self
        
        //self.table1.register(TableViewCellCustom.self , forCellReuseIdentifier: "CELL_ZA_TABELU1")
        //self.table2.register(UITableViewCell.self, forCellReuseIdentifier: "CELL_ZA_TABELU2")

        //table1.reloadData()
        table2.reloadData()
        
        print("TABLE LOADED")
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(" view did appear EVENT")
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////
    // MARK: - Table view data source
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       // return 1
          return meals.count
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        
        //var cell = HUD_inTable();
        var cell_normal = UITableViewCell();
        let meal = meals[(indexPath as NSIndexPath).row]
        
        if tableView == self.table1
        {
        print("table1 - cellatRow")
          
        let cellIdentifier = "CELL_ZA_TABELU1"
        var cell1 = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TableViewCellCustom
        
        // Fetches the appropriate meal for the data source layout.
        let meal = meals[(indexPath as NSIndexPath).row]
        
            
        if(cell1.title != nil)
        
           {
              print("access custom cell OK")
              cell1.title.text = meal.name
              cell1.photo.image = meal.photo
 
            }
          
           return cell1
        }
        else {
         
        // HANDLE TABLE2
        print("TABLE2")
            
            // WORKS HERE with no custom cell
            let cellIdentifier = "CELL_ZA_TABELU2"
            cell_normal = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! UITableViewCell
            
            let meal = meals[(indexPath as NSIndexPath).row]
            
            cell_normal.textLabel?.text = meal.name
            cell_normal.detailTextLabel?.text = "fuck this"
            cell_normal.imageView?.image = meal.photo
            
            
            return cell_normal
        }
        
     
     
    }
    
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //CODE TO BE RUN ON CELL TOUCH
        
        if (tableView == self.table1)
        {
        
          print("CLICK EVENT ON " , indexPath.row )
            
        }
        else if (tableView == self.table2) {
           
          print("CLICK EVENT ON " , indexPath.row )
            
        }
        
        
        
     }
    
   
}



