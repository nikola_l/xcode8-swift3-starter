//
//  TableViewCell.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/6/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import UIKit

class TableViewCellCustom: UITableViewCell {

  
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var photo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
