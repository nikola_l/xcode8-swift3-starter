//
//  webBrowser_XIB.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/7/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import UIKit

class webBrowser_XIB: UIViewController {
    
    
    @IBOutlet weak var imageFromWeb1: UIImageView!
    @IBOutlet weak var imageFromWeb2_ASYN: UIImageView!
    @IBOutlet weak var BrowserControl: UIWebView!
    @IBOutlet weak var addressBarBTN: UIButton!
    @IBOutlet weak var loadTextHTML_BTN: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        LoadPage(address: "http://google.com")
        
        
        var NIKO =  DEVICE_INFO()
        print ( "this is width - 10% of screen  : " ,   NIKO.W(per: 10) )
        
        
        print (  UIDevice().modelName)
        
        imageFromWeb1.downloadFrom(link: "https://static.kupindoslike.com/SRS-predizborni-plakat-king-size-_slika_L_39382377.jpg")
        imageFromWeb2_ASYN.downloadFromA(urlString: "https://static.kupindoslike.com/SRS-predizborni-plakat-king-size-_slika_L_39382377.jpg")
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    ////////////////////////////////
    // LOAD WEB PAGE
    ////////////////////////////////
    func LoadPage ( address : String ) {
        
        
        let url = NSURL (string: address);
        let requestObj = NSURLRequest(url: url! as URL);
        BrowserControl.loadRequest(requestObj as URLRequest);
        
        
    }
    
    
    @IBAction func loadLocalHTML(_ sender: AnyObject) {
        
        let localfilePath = Bundle.main.url(forResource: "localHTML", withExtension: "html");
        let myRequest = NSURLRequest(url: localfilePath!);
        BrowserControl.loadRequest(myRequest as URLRequest);
        
        
    }
    
    @IBAction func loadText(_ sender: AnyObject) {
        
        let htmlString:String! = "<br /><h2>Welcome to SourceFreeze!!!</h2>"
        BrowserControl.loadHTMLString(htmlString, baseURL: nil)
        
        
    }
    
}
