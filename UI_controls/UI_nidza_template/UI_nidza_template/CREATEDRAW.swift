//
//  CREATEDRAW.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/8/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import UIKit
class ResuableCustomView: UIView {
    
    @IBOutlet var view: UIView!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
       let XIB_ = Bundle.main.loadNibNamed("CREATEDRAW", owner: self, options: nil)?[0] as! UIView
        self.addSubview(XIB_)
        XIB_.frame = self.bounds
    }
}
