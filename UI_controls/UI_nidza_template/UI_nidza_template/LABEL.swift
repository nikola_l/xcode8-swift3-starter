//
//  UI_controls_generator.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 8/31/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import Foundation
import UIKit

class LABEL  {
    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "nib file name", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    var SELF : UIView!
    var SELF_SCROLL : UIScrollView!
    var text : String!
    var X : CGFloat!
    var Y : CGFloat!
    
    
    let BTN  = UILabel() as UILabel
    
    // CONSTRUCT INIT
    init ( UIView_ :  UIView , text_ : String ) {
        
        print("label instance constructor1")
        self.SELF = UIView_;
        self.text  = text_;
        
        
        
    }
    
    init ( UIScroll :  UIScrollView , text_ : String ) {
        
        print("label instance constructor2")
        SELF_SCROLL = UIScroll;
        text  = text_;
        
    }
    
    init ( UIView_ :  UIScrollView , text_ : String , X_ : CGFloat , Y_ : CGFloat  ) {
        
        print("label instance constructor2")
        SELF = UIView_;
        text  = text_;
        X = X_;
        Y = Y_;
        
        CREATE_LABEL_FULL()
        
    }
    
    
    
    func CREATE_LABEL_FULL () {
        
        
        
        BTN.text = text
        
        BTN.backgroundColor = UIColor.red
        BTN.frame.origin.x = X
        BTN.frame.origin.y = Y
        BTN.frame.size = CGSize(width: 255, height: 75 )
        BTN.frame.origin = CGPoint(x: X, y: Y)
        BTN.textColor = UIColor.lightGray
        
        SELF.addSubview(BTN)
        
        //let image = UIImage(named: "name") as UIImage?
        
        //BTN.setImage(image, forState: .Normal)
        //BTN.addTarget(self, action: "btnTouched:", forControlEvents:.TouchUpInside)
        
        
    }
    
    
    
    func CREATE_LABEL () {
        
        
        
        //let BTN = UIButton()
        
        BTN.text = text
        
        BTN.backgroundColor = UIColor.red
        BTN.frame.origin.x = 10
        BTN.frame.origin.y = 100
        BTN.frame.size = CGSize(width: 155, height: 155 )
        BTN.frame.origin = CGPoint(x: 10, y: 20)
        BTN.textColor = UIColor.lightGray
        SELF.addSubview(BTN)
        
        //let image = UIImage(named: "name") as UIImage?
        
        //BTN.setImage(image, forState: .Normal)
        //BTN.addTarget(self, action: "btnTouched:", forControlEvents:.TouchUpInside)
        
        
    }
    
    
    
}
