//
//  PickCameraXIB.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 8/31/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

//
//  ViewController.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 8/29/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//
import UIKit
import MobileCoreServices
import AVFoundation


class PickCamera: UIViewController , UINavigationControllerDelegate , UIImagePickerControllerDelegate
 
 {
    
    @IBOutlet weak var PREVIEW: UIImageView!
    @IBOutlet weak var startBtn: UIButton!
    
    var imagePicker = UIImagePickerController()
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        print("PICK A CAMERA VIEW CONTROLLER LOADED ")
    
        
    }
  
    
    @IBAction func startBtn_event(_ sender: AnyObject) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            print("Open gallery!")
            
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        print("Image is selected !!! nice")
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            
            dismiss(animated: true, completion: {
                self.PREVIEW.image =  pickedImage
            } )
            
            
        }else{
            
            dismiss(animated: true, completion: nil)
            
        }
    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
  
}
