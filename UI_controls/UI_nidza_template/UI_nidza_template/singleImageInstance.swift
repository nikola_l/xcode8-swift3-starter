//
//  singleImageInstance.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 8/31/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import Foundation
import UIKit

class IMAGE_BOX   {
    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "nib file name", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    var SELF :UIView
    var imageFromProgram : UIImage!
    
    // CONSTRUCT INIT
    init ( UIView_ :  UIView , imgFromProgram_ : UIImage ) {
        
        print("Image control instance constructor")
        SELF = UIView_;
        imageFromProgram = imgFromProgram_;
        
    }
    
    
    
    func CREATE_IMAGE () {
        
        let IMG  = UIImageView()
        //BTN.setTitle(buttonText, for: UIControlState.normal )
        IMG.backgroundColor = UIColor.red
        IMG.frame.origin.x = 10
        IMG.frame.origin.y = 100
        IMG.frame.size = CGSize(width: 155, height: 155 )
        IMG.frame.origin = CGPoint(x: 10, y: 20)
        
        //IMG.setTitleColor( UIColor.black  , for: UIControlState.normal )
        IMG.image = imageFromProgram
        SELF.addSubview(IMG)
        
        
        
    }
    
    
    
}
