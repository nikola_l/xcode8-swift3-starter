//
//  UI_controls_generator.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 8/31/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import Foundation
import UIKit

class BUTTON   {
    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "nib file name", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    var SELF :UIView
    var buttonText : String
    
    let BTN  = UIButton(type: UIButtonType.custom) as UIButton
    
    // CONSTRUCT INIT
    init ( UIView_ :  UIView , buttonText_ : String ) {
        
        print("Button instance constructor")
        SELF = UIView_;
        buttonText = buttonText_;
        
    }
    
    
    
    func CREATE_BUTTON () {
        
        
        
        //let BTN = UIButton()
        
        BTN.setTitle(buttonText, for: UIControlState.normal )
        BTN.backgroundColor = UIColor.red
        BTN.frame.origin.x = 10
        BTN.frame.origin.y = 100
        BTN.frame.size = CGSize(width: 155, height: 155 )
        BTN.frame.origin = CGPoint(x: 10, y: 20)
        BTN.setTitleColor( UIColor.black  , for: UIControlState.normal )
        SELF.addSubview(BTN)
        
        //let image = UIImage(named: "name") as UIImage?
        
        //BTN.setImage(image, forState: .Normal)
        //BTN.addTarget(self, action: "btnTouched:", forControlEvents:.TouchUpInside)
        
        
    }
    
    
    
}
