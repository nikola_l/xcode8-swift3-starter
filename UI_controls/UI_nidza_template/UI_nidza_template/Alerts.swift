//
//  Alerts.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/12/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import Foundation
import UIKit


class ALERT {
    
    func currentVC() -> UIViewController? {
        
        // guard let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController else { return nil }
        //  guard let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UIViewController else { return nil }
        
        // let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        //let viewController = appDelegate.window!.rootViewController! as UIViewController
        
        
        let viewController =  UIApplication.shared.keyWindow?.rootViewController
        
        return viewController
    }
    
    // CONSTRUCT INIT
    init (title : String , msg : String) {
        
        print("ALERT instance constructor")
        let uiAlert = UIAlertController(title: title , message: msg, preferredStyle: UIAlertControllerStyle.alert);
        
        self.currentVC()?.present(uiAlert,animated: true, completion: nil);
        
        print("ALERT instance constructor")
        //self.currentVC()?
        
        uiAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            
        print("Click of default button");
            
        }))
        
        uiAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        print("Click of cancel button");
            
        }))
        
    }
    

}
