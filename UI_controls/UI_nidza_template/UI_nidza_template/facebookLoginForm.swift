//
//  facebookLoginForm.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/9/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class facebookLoginForm: UIViewController, FBSDKLoginButtonDelegate, UITextFieldDelegate
    
{
   
  
     let loginView : FBSDKLoginButton = FBSDKLoginButton()
    let fbLoginManager = FBSDKLoginManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       // fbLoginManager.loginBehavior = FBSDKLoginBehavior.systemAccount
       // fbLoginManager.loginBehavior = FBSDKLoginBehavior.native
         fbLoginManager.loginBehavior = FBSDKLoginBehavior.web
      // FBSDKLoginManager.loginB
        
        
        self.view.addSubview(loginView)
        loginView.center = self.view.center
        loginView.readPermissions = ["public_profile", "email", "user_friends"]
        loginView.delegate = self
        
        // If we have an access token, then let's display some info
        
        if (FBSDKAccessToken.current() != nil)
        {
            // Display current FB premissions
            print (FBSDKAccessToken.current().permissions)
            
            // Since we already logged in we can display the user datea and taggable friend data.
            
            self.showUserData()
            self.showFriendData()
            
        }
        else {
        print("no log at start")
        }
        
        
        
        let CButtonFB = BUTTON(UIView_: self.view , buttonText_: "FB LOGIN" );
        CButtonFB.CREATE_BUTTON()
        CButtonFB.BTN.addTarget(self, action:#selector(self.selected), for: .touchUpInside)
        
        
    }
    
    func selected(_ sender: AnyObject) {
        
        print("return data")
        
        //CUSTOM TEST 
        
        
        if (FBSDKAccessToken.current() != nil)
        {
            // Display current FB premissions
            print (FBSDKAccessToken.current().permissions)
            print("OK OK")
            // Since we already logged in we can display the user datea and taggable friend data.
            
            self.showUserData()
            self.showFriendData()
            
            
        }
        else {
    
        var login = FBSDKLoginManager()
        // login.loginBehavior = FBSDKLoginBehavior.native
        login.loginBehavior = FBSDKLoginBehavior.web
        // login.logOut()
        login.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self ) {
            (loginResult: FBSDKLoginManagerLoginResult?, error: Error?) in
            
            if (error != nil) {
                print("Process error2")
            }
            else if (loginResult?.isCancelled)! {
                print("Cancelled2")
            }
            else {
                print("Logged in2")
                     self.showUserData()
            }
            
        }
        

        }
   
        
        
        
    }
    
    
    //#########################
    
    func showUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "id, name, gender, first_name, last_name, locale, email"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
            }
            else
            {
                 let data:[String:AnyObject] = result as! [String : AnyObject]
                
                let userName : NSString = data["name"] as! NSString
                print("User Name is: \(userName)")
                
                if let userEmail : NSString = data["email"] as? NSString {
                    print("User Email is: \(userEmail)")
                }
            }
        })
    }
    
    //#########################
     
    
    func showFriendData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me/taggable_friends?limit=999", parameters: ["fields" : "name"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
            }
            else
            {
                
                let data:[String:AnyObject] = result as! [String : AnyObject]
                if let friends : NSArray = data["data"] as? NSArray{
                    var i = 1
                    for obj in friends {
                        print(obj)
                      //  if let name = obj["name"] as? String {
                      //      print("\(i) " + name)
                      //      i += 1;
                      //  }
                    }
 
                }
           
                
                
            }
        })
    }
    
    //########## for delegate 
    
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        
        
        if ((error) != nil)
        {
            // Process error
            print("error")
            print(error)
        }
        else if result.isCancelled {
            // Handle cancellations
            print("cancel!")
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            print("Cool")
            if result.grantedPermissions.contains("email")
            {
                // Do work
                print("email permision oK")
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
    
    //###################
}
