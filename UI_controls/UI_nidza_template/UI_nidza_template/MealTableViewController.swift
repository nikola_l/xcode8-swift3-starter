//
//  MealTableViewController.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 9/2/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import UIKit

class MealTableViewController: UITableViewController  
 {
// MARK: Properties
    var meals = [Meal]()
    
    func loadSampleMeals() {
        let photo1 = UIImage(named: "fire1_1")!
        let meal1 = Meal(name: "Caprese Salad", photo: photo1, rating: 4)!
        
        let photo2 = UIImage(named: "fire1_2")!
        let meal2 = Meal(name: "Chicken and Potatoes", photo: photo2, rating: 5)!
        
        let photo3 = UIImage(named: "fire1_3")!
        let meal3 = Meal(name: "Pasta with Meatballs", photo: photo3, rating: 3)!
        
        meals += [meal1, meal2, meal3]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

       loadSampleMeals()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meals.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        
        let cellIdentifier = "HUD"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! HUD_inTable
        
        // Fetches the appropriate meal for the data source layout.
        let meal = meals[(indexPath as NSIndexPath).row]
        cell.title.setTitle(meal.name, for: UIControlState.normal)
        cell.photo.image = meal.photo
        return cell
        
    }

}
