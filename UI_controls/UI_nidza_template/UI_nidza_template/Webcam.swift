//
//  Webcam.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 8/30/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import Foundation

// Import this
import AVFoundation
import UIKit
import MobileCoreServices


protocol PickerProtocol : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
}

class CAM_DEVICE : NSObject ,
    PickerProtocol , UINavigationControllerDelegate,
    UIImagePickerControllerDelegate
{
    
    
    let captureSession = AVCaptureSession()
    // If we find a device we'll store it here for later use
    var captureDevice : AVCaptureDevice?
    var SELF : UIViewController;
    let imagePicker = UIImagePickerController()
    
    init ( UIView_ :  UIViewController  ) {
        
        print("Cam device instance constructor")
        SELF = UIView_;
        
    }
    
    func START (){
        
        // captureSession.sessionPreset = AVCaptureSessionPresetLow
        //   let devices = AVCaptureDevice.devices()
        //  print(devices)
        
        /*
         DispatchQueue.main.asynchronously(execute: {
         let imagePicker: UIImagePickerController = UIImagePickerController();
         imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
         imagePicker.mediaTypes = [kUTTypeImage];
         imagePicker.allowsEditing = false;
         imagePicker.delegate = self;
         
         if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){ // on a tablet, the image picker is supposed to be in a popover
         let popRect: CGRect = buttonRect;
         let popover: UIPopoverController = UIPopoverController(contentViewController: imagePicker);
         popover.presentPopoverFromRect(popRect, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Up, animated: true);
         }else{
         self.presentViewController(imagePicker, animated: true, completion: nil);
         }
         });
         */
        
        
        //@
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
            
            
            imagePicker.delegate = self
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
            imagePicker.allowsEditing = true
            
            SELF.present(imagePicker, animated: true, completion: nil)
            
        }
        
        //@
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
            
            print("Image is selected !!! nice")
            /*
             if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
             
             SELF.dismiss(animated: true, completion: {
             
             //self.PREVIEW.image =  pickedImage
             print ("POGODAT !!!!!!")
             
             
             } )
             
             
             }else{
             
             SELF.dismiss(animated: true, completion: nil)
             
             }
             */
        }
        
        //@
        /*
         
         func imagePickerController(picker: UIImagePickerController , didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
         
         print("DUDUDUDUDU")
         //imagePicked.image = image
         
         SELF.dismiss(animated: true, completion: nil);
         }
         
         //@
         */
        
        
        //@
        
        
        
        
    }
    
    
    
    
    
    
}
