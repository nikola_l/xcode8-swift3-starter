//
//  ViewController.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 8/29/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//
import UIKit
import MobileCoreServices


class ViewController: UIViewController ,
 UINavigationControllerDelegate ,
 UIImagePickerControllerDelegate {

    
    //UINavigationControllerDelegate
    //UIImagePickerControllerDelegate
    
    
    // Pre Def in desinger - dragdrop from storyboard
    @IBOutlet weak var AnimationBtnShow: UIButton!
    @IBOutlet weak var InstanceAniBTN: UIButton!
    @IBOutlet weak var CamBTN: UIButton!
    @IBOutlet weak var createBTN: UIButton!
    @IBOutlet weak var IMG: UIImageView!
    @IBOutlet weak var gotoPickCamera: UIButton!
    @IBOutlet weak var gotoTableViewForm: UIButton!
    @IBOutlet weak var gotoTableOnForm: UIButton!
    @IBOutlet weak var webBrowserBTN: UIButton!
    @IBOutlet weak var facebookBTN: UIButton!
    
    @IBOutlet weak var faceBook2: UIButton!
    @IBOutlet weak var gotoCanvasForm: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // ONLOAD
        
    
    }

    
    @IBAction func ShowAni(_ sender: AnyObject) {
        // FIRST BTN
        Load_newForm()
        print("ACTION DONE CREATE ANIMATION IN CURRENT FORM ")
        
    }
    
  
    @IBAction func MakeINstanceANimation(_ sender: AnyObject) {
        
         let ANI1 = ANI( UIView_: self.view , sequence_frames: 3 )
         ANI1.CREATE_ANIMATION()
        
         print("ACTION DONE MAKE INSTANCE ANIMATION")
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    /////////////////////////////////////////////////////////////
    // SWITCH view : not working always
    /////////////////////////////////////////////////////////////
    func Load_newForm () {
        // NEW FORM 
        let AniView:AnimationClassView  = AnimationClassView()
        self.present(AniView, animated: true, completion: nil)
        
    }
    
    /////////////////////////////////////////////////////////////
    // INSTANCE OF CAMERA class
    /////////////////////////////////////////////////////////////
    @IBAction func getCam(_ sender: UIButton) {
        
        print("ACTION : CAM ACCESS")
        let CAM1 = CAM_DEVICE( UIView_: self )
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            //put your code which should be executed with a delay here
        CAM1.START()
        }
        
        
        
    }
    
    
    /////////////////////////////////////////////////////////////
    //CREATE BUTTON IN RUNTIME
    /////////////////////////////////////////////////////////////
    @IBAction func createButton(_ sender: UIButton) {
        
        let MyButton1 = BUTTON(  UIView_ : self.view ,  buttonText_ : "Nidza")
        MyButton1.CREATE_BUTTON()
        
    }
    
 
    /////////////////////////////////////////////////////////////
    //GOTO FORM WITH CAMERA CODE
    /////////////////////////////////////////////////////////////
    @IBAction func gotoPickCamera_action(_ sender: AnyObject) {
        
       switchToViewController(identifier: "PickCamera1")
       
        /*
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PickCamera1") as UIViewController
        self.present(viewController, animated: false, completion: nil)
       */
        
    }
    
    
    @IBAction func gotoMp3Player(_ sender: AnyObject) {
        
        
        switchToViewController(identifier: "mp3Player")
        
        
    }
  
    @IBAction func gotoCanvas(_ sender: AnyObject) {
        
        switchToViewController(identifier: "canvas_form")
        
        
    }
    
    
    @IBAction func gotoTableOnform_(_ sender: AnyObject) {
        
        
         switchToViewController(identifier: "TableXIB1")
        
    }
    
    
    
      /////////////////////////////////////////////////////////////
      //SWITCH TO NEW VIEW - best for this action
      /////////////////////////////////////////////////////////////
        func switchToViewController(identifier: String  ) {
            
            let viewController1:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier) as UIViewController
            self.present(viewController1, animated: false, completion: nil)
            
        }
    
    
    /////////////////////////////////////////////////////////////
    //SWITCH TO TABLE VIEW FORM
    /////////////////////////////////////////////////////////////
    @IBAction func gotoTableViewForm(_ sender: AnyObject) {
        
         switchToViewController(identifier: "tableViewControlerXIB")
        
    }
    
    //webBrowser
    @IBAction func gotoWebBrowser(_ sender: AnyObject) {
        
        switchToViewController(identifier: "webBrowser")
        
    }
    
    
    @IBAction func gotofacebook(_ sender: AnyObject) {
        
    
        switchToViewController(identifier: "FBLogin")

        
    }
    
    
    @IBAction func gotoFacebook2(_ sender: AnyObject) {
        
        switchToViewController(identifier: "FBLogin2")
        
    }
    
    // must setup pList also IMPORTANT!
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
}
    
