//
//  AnimationClass.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 8/29/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//
import UIKit

class AnimationClassView: UIViewController {
    
    @IBOutlet weak var BACK_BUTTON: UIButton!
 
    /////////////////////////
    // ONLOAD
    /////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view, typically from a nib.
        // THIS IS class with view , no instance of this class...
        // just for quick example 
        print(" LOG : AnimationClass onload event")
        
        let Animation_Box  = UIImageView()
        
        var images: [UIImage] = []
        
        for i in 1...10 {
            images.append(UIImage(named: "fire1_\(i)")!)
        }
        
       Animation_Box.animationImages = images
       Animation_Box.animationDuration = 1.0
       Animation_Box.frame.origin.x = 10
       Animation_Box.frame.origin.y = 10
       Animation_Box.frame.size = CGSize(width: 95, height: 95 )
       Animation_Box.frame.origin = CGPoint(x: 150, y: 250)
        
       self.view.addSubview(Animation_Box)
        
       Animation_Box.startAnimating()

        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BACK(_ sender: AnyObject) {
        
        let viewController1:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MAIN_FORM") as UIViewController
        self.present(viewController1, animated: false, completion: nil)
        
    }
    
    
}
