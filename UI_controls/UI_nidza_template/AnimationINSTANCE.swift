//
//  AnimationINSTANCE.swift
//  UI_nidza_template
//
//  Created by Nikola Lukic on 8/30/16.
//  Copyright © 2016 Nikola Lukic. All rights reserved.
//

import Foundation
import UIKit

class ANI   {
    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "nib file name", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    
    var SELF :UIView;
    var NumberOfFrames : Int = 0;
    
    
    init ( UIView_ :  UIView , sequence_frames : Int ) {
        
        print("Animation test instance constructor")
        SELF = UIView_;
        NumberOfFrames = sequence_frames;
        
        
    }
    
    
    
    func CREATE_ANIMATION () {
        
        let Animation_Box  = UIImageView()
        
        var images: [UIImage] = []
        for i in 1...NumberOfFrames {
            images.append(UIImage(named: "fire1_\(i)")!)
        }
        
        Animation_Box.animationImages = images
        Animation_Box.animationDuration = 1.0
        Animation_Box.frame.origin.x = 10
        Animation_Box.frame.origin.y = 10
        Animation_Box.frame.size = CGSize(width: 55, height: 55 )
        Animation_Box.frame.origin = CGPoint(x: 10, y: 20)
        
        SELF.addSubview(Animation_Box)
        
        Animation_Box.startAnimating()
        
    }
    
    
    
}
